import { fetchAction } from "./actions/fetchAction";
import { RESOURCE_ATTACH, RESOURCE_FETCH, RESOURCE_FETCH_SUCCESS, RESOURCE_FETCH_FAILURE } from "./reducer";
import { IFetchAction } from "./interfaces/IFetchAction";
import { IAttachAction } from "./interfaces/IAttachAction";

export const service = (store: any) => (next: any) => (action: IFetchAction | IAttachAction) => {
  next(action);

  if (action.type === RESOURCE_ATTACH || action.type === RESOURCE_FETCH) {
    const key = action.payload.key;
    const dao = action.payload.dao;
    const data = store.getState().resource[key];

    switch (action.type) {
      case RESOURCE_ATTACH:
        if (data.meta.instances === 1) {
          store.dispatch(fetchAction(dao));
        }
        break;

      case RESOURCE_FETCH:
        if (data) {
          store.dispatch(dao.fetch()).then(
            (data: any) => store.dispatch({ type: RESOURCE_FETCH_SUCCESS, payload: { key, data } }),
            (error: any) => store.dispatch({ type: RESOURCE_FETCH_FAILURE, payload: { key, error } })
          );
        }
        break;
    }
  }
};