export { reducer } from "./reducer";
export { service } from './service';
export { useResource } from './hooks/useResource';

export { IReduxDAO } from './interfaces/IReduxDAO';