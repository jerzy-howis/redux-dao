import { Dispatch } from 'redux';

export interface IReduxDAO<T> {
  getKey: () => string;
  fetch: () => (dispatch: Dispatch<any>, getState: () => any) => Promise<T>;
  remove: () => (dispatch: Dispatch<any>, getState: () => any) => Promise<any>;
  update: (params: any) => (dispatch: Dispatch<any>, getState: () => any) => Promise<undefined>;
}