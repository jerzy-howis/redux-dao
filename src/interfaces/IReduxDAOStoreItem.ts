import { IReduxDAOStoreRequest } from "./IReduxDAOStoreRequest";

export interface IReduxDAOStoreItem<T> {
  data: T,
  meta: {
    key: string;
    instances: number;
  },
  request: IReduxDAOStoreRequest;
};