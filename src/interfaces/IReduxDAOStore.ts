import { List } from "../types/List";
import { IReduxDAOStoreItem } from "./IReduxDAOStoreItem";

export type IReduxDAOStore<T> = List<IReduxDAOStoreItem<T>>;