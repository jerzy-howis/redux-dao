export interface IReduxDAOStoreRequest {
  status: string;
  error: number | null;
  message: string | null;
}