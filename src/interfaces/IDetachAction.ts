import { IReduxDAO } from "./IReduxDAO";

export interface IDetachAction<T=any> {
  type: string;
  payload: {
    key: string;
    dao: IReduxDAO<T>
  }
}