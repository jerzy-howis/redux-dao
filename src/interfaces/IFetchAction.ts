import { IReduxDAO } from "./IReduxDAO";

export interface IFetchAction<T=any> {
  type: string;
  payload: {
    key: string;
    dao: IReduxDAO<T>
  }
}