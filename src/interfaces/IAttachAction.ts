import { IReduxDAO } from "./IReduxDAO";

export interface IAttachAction<T=any> {
  type: string;
  payload: {
    key: string;
    dao: IReduxDAO<T>
  }
}