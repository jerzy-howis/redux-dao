import { RESOURCE_DETACH } from '../reducer';
import { IReduxDAO } from '../interfaces/IReduxDAO';
import { IDetachAction } from '../interfaces/IDetachAction';

export const detachAction = <T = any>(dao: IReduxDAO<T>) : IDetachAction => ({
  type: RESOURCE_DETACH,
  payload: {
    dao,
    key: dao.getKey()
  }
});