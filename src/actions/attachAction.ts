import { RESOURCE_ATTACH } from '../reducer';
import { IReduxDAO } from '../interfaces/IReduxDAO';
import { IAttachAction } from '../interfaces/IAttachAction';

export const attachAction = <T = any>(dao: IReduxDAO<T>) : IAttachAction => ({
  type: RESOURCE_ATTACH,
  payload: {
    dao,
    key: dao.getKey()
  }
});
