import { RESOURCE_FETCH } from '../reducer';
import { IReduxDAO } from '../interfaces/IReduxDAO';
import { IFetchAction } from '../interfaces/IFetchAction';

export const fetchAction = <T=any>(dao: IReduxDAO<T>) : IFetchAction => ({
  type: RESOURCE_FETCH,
  payload: {
    dao,
    key: dao.getKey()
  }
});