import { AnyAction } from "redux";
import { IReduxDAOStore } from "./interfaces/IReduxDAOStore";

export const RESOURCE_ATTACH = '@@redux-dao/attach';
export const RESOURCE_DETACH = '@@redux-dao/detach';

export const RESOURCE_FETCH = '@@redux-dao/fetch';
export const RESOURCE_FETCH_SUCCESS = '@@redux-dao/fetch-success';
export const RESOURCE_FETCH_FAILURE = '@@redux-dao/fetch-failure';

export const RESOURCE_UPDATE = '@@redux-dao/update';
export const RESOURCE_UPDATE_SUCCESS = '@@redux-dao/update-success';
export const RESOURCE_UPDATE_FAILURE = '@@redux-dao/update-failure';

export const RESOURCE_DELETE = '@@redux-dao/delete';
export const RESOURCE_DELETE_SUCCESS = '@@redux-dao/delete-success';
export const RESOURCE_DELETE_FAILURE = '@@redux-dao/delete-failure';

export const initial = {
  data: null,
  request: {
    status: 'LOADING',
    error: null,
    message: null
  }
};

export const reducer = (state: IReduxDAOStore<any> = {}, action: AnyAction) => {
  if (action.type.indexOf('@@redux-dao/') !== 0) {
    return state;
  }

  let key = <string>action.payload.key;
  let data = state[key] || null;

  if (data === null && action.type === RESOURCE_ATTACH) {
    data = { ...initial, meta: { key, instances: 0 } };
  }

  switch (action.type) {
    case RESOURCE_ATTACH: data && data.meta.instances++; break;
    case RESOURCE_DETACH: data && data.meta.instances--; break;

    case RESOURCE_FETCH:
      if (data) {
        data.data = data.data || null;
        data.request = { status: data.data ? 'RELOADING' : 'LOADING', error: null, message: null };
      }
      break;

    case RESOURCE_FETCH_SUCCESS:
      if (data) {
        data.data = action.payload.data;
        data.request = { status: 'SUCCESS', error: null, message: null };
      }
      break;

    case RESOURCE_FETCH_FAILURE:
      if (data) {
        data.data = null;
        data.request = { status: 'ERROR', error: action.payload.error.code, message: action.payload.error.message };
      }
      break;
  }

  const { [key]: old, ...result } = { ...state };

  if (data && data.meta.instances > 0) {
    result[key] = { ...data };
  } 

  return result;
}
