import { initial } from '../reducer';
import { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";

import { List } from '../types/List';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';

import { IReduxDAO } from '../interfaces/IReduxDAO';
import { IReduxDAOStore } from '../interfaces/IReduxDAOStore';
import { IReduxDAOStoreRequest } from '../interfaces/IReduxDAOStoreRequest';

import { fetchAction } from '../actions/fetchAction';
import { attachAction } from '../actions/attachAction';
import { detachAction } from '../actions/detachAction';

export const useResource = <T>(getDao: (params: List<any>) => IReduxDAO<T>, params: List<any> = {}, active: boolean = true) => {
  const dao = getDao(params);
  const key = dao.getKey();
  const dispatch = useDispatch() as ThunkDispatch<{}, undefined, Action>;
  const resource = useSelector((state: { resource: IReduxDAOStore<any> }) => state.resource[key] || initial);

  const fetch = useCallback(() => {
    dispatch(fetchAction(dao));
  }, [key]);

  const remove = () => dispatch(dao.remove()).then(fetch);
  const update = (params: List<any>) => dispatch(dao.update(params)).then(fetch);

  useEffect(() => {
    if(active) {
      dispatch(attachAction(dao));
    }

    return () => {
      if(active) {
        dispatch(detachAction(dao));
      }
    }
  }, [key, active]);

  return [resource.data, resource.request, { fetch, update, remove }] as [T, IReduxDAOStoreRequest, {
    fetch: typeof fetch,
    update: typeof update,
    remove: typeof remove
  }];
}